package br.com.jdt.factory;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import br.com.jdt.bdUtil.Paginacao;
import br.com.jdt.bdUtil.SessionFactoryUtil;
import br.com.jdt.modelagem.Usuario;

public class UsuarioDAO {

	public static Usuario get(Integer id){
	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();

			Usuario usuario = (Usuario)session.createCriteria(Usuario.class)
		    .add( Property.forName("id").eq(id) )
		    .uniqueResult();

	      tx.commit();
	      return usuario;
	    }catch (RuntimeException e) {
	    	e.printStackTrace();
	      if (tx != null && tx.isActive()) {
	        try {
	// Second try catch as the rollback could fail as well
	          tx.rollback();
	        } catch (HibernateException e1) {
	          e.printStackTrace();
	        }
	// throw again the first exception
	        throw e;
	      }
	    }
	    return null;
	}
	
	public static  Usuario getUsuario(String login, String senha){
	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();

			Usuario usuario = (Usuario)session.createCriteria(Usuario.class)
		    .add( Property.forName("login").eq(login) )
		    .add( Property.forName("senha").eq(senha) )
		    .uniqueResult();

	      tx.commit();
	      return usuario;
	    }catch (RuntimeException e) {
	    	e.printStackTrace();
	      if (tx != null && tx.isActive()) {
	        try {
	// Second try catch as the rollback could fail as well
	          tx.rollback();
	        } catch (HibernateException e1) {
	          e.printStackTrace();
	        }
	// throw again the first exception
	        throw e;
	      }
	    }
	    return null;
	}

	public static Integer find(String busca) {
	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();

	      Criteria crit = session.createCriteria( Usuario.class )
	      .add( Restrictions.or( Restrictions.ilike("nome", "%" + busca + "%")  , Restrictions.ilike("login", "%" + busca + "%") ))
  		  .setProjection(Projections.rowCount());

	      Integer valor = Integer.valueOf(crit.uniqueResult().toString());

	      tx.commit();
	      return valor;
	    }catch (RuntimeException e) {
	    	e.printStackTrace();
	      if (tx != null && tx.isActive()) {
	        try {
	// Second try catch as the rollback could fail as well
	          tx.rollback();
	        } catch (HibernateException e1) {
	          e.printStackTrace();
	        }
	// throw again the first exception
	        throw e;
	      }
	    }
	    return null;
	}

	public static List<Usuario> find(String busca, Paginacao paginacao) {
	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();

	      List<Usuario> lista = session.createCriteria( Usuario.class )
	      .add( Restrictions.or( Restrictions.ilike("nome", "%" + busca + "%")  , Restrictions.ilike("login", "%" + busca + "%") ) )
	      .setFirstResult( paginacao.getFirstResult() )
	      .setMaxResults( paginacao.getQtdePorPagina() )
	      .list();

	      tx.commit();
	      return lista;
	    }catch (RuntimeException e) {
	    	e.printStackTrace();
	      if (tx != null && tx.isActive()) {
	        try {
	// Second try catch as the rollback could fail as well
	          tx.rollback();
	        } catch (HibernateException e1) {
	          e.printStackTrace();
	        }
	// throw again the first exception
	        throw e;
	      }
	    }
	    return null;
	}
	
}
