package br.com.jdt.Controller;

import java.util.List;

import br.com.jdt.bdUtil.Paginacao;
import br.com.jdt.factory.UsuarioDAO;
import br.com.jdt.modelagem.Usuario;

import com.opensymphony.xwork2.ActionSupport;

public class UsuarioController extends ActionSupport{
	
	String id;
	Usuario usuario;
	boolean editar=false;

	String pagina;
	String busca;
	Paginacao paginacao;
	List<Usuario> usuarios;
	
	String nome;
	String login;
	String email;
	String senha;
	
	//Variaveis para o validaFormErro e validaFormSucesso
	String informacao;
	String resposta;
	
	public String add() throws Exception{
		if( id != null)
			usuario = UsuarioDAO.get(Integer.valueOf(id));
		else
			usuario = new Usuario();
		
		usuario.setNome(nome);
		usuario.setLogin(login);
		usuario.setEmail(email);
		if(!senha.trim().equals(""))
			usuario.setSenha(senha);
		
		usuario.save();
		
		return SUCCESS;
	}
	
	public String list() throws Exception {
    	busca = busca==null ? "" : busca;

    	paginacao = new Paginacao(10,  UsuarioDAO.find( busca ) , pagina);
    	usuarios = UsuarioDAO.find(busca, paginacao) ;

		return SUCCESS;
    }
	
	public String edit() throws Exception {
		if(id!=null){
			usuario = UsuarioDAO.get(Integer.valueOf(id));
			editar=true;
			return SUCCESS;
		}
		return ERROR;
	}
	
	public String delete() throws Exception{
		if( id!=null){
			usuario = UsuarioDAO.get(Integer.valueOf(id));
			usuario.delete();
			return SUCCESS;
		}
		return ERROR;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public String getPagina() {
		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	public String getBusca() {
		return busca;
	}

	public void setBusca(String busca) {
		this.busca = busca;
	}

	public Paginacao getPaginacao() {
		return paginacao;
	}

	public void setPaginacao(Paginacao paginacao) {
		this.paginacao = paginacao;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getInformacao() {
		return informacao;
	}

	public void setInformacao(String informacao) {
		this.informacao = informacao;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
}
