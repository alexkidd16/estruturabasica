package br.com.jdt.modelagem;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.jdt.bdUtil.SessionFactoryUtil;

public class Modelo{

	  public void delete() {

	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();
	      session.delete(this);
	      tx.commit();
	    } catch (RuntimeException e) {
	      if (tx != null && tx.isActive()) {
	        try {
	// Second try catch as the rollback could fail as well
	          tx.rollback();
	        } catch (HibernateException e1) {
	          //logger.debug("Error rolling back transaction");
	        }
	// throw again the first exception
	        throw e;
	      }
	    }
	  }
	  
	  public void save() {
	    Transaction tx = null;
	    Session session = SessionFactoryUtil.getInstance().getCurrentSession();
	    try {
	      tx = session.beginTransaction();
	      session.saveOrUpdate(this);
	      tx.commit();
	    } catch (RuntimeException e) {
	    	e.printStackTrace();
	      if (tx != null && tx.isActive()) {
	        try {
	        	//Second try catch as the rollback could fail as well
	        	tx.rollback();
	        } catch (HibernateException e1) {
	          e1.printStackTrace();
	        }
	        // throw again the first exception
	        throw e;
	      }
	    }
	  }
}
