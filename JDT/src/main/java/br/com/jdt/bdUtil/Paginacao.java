package br.com.jdt.bdUtil;

public class Paginacao {

	Integer[] links;
	int paginaAtual;
	int firstResult;
	int qtdePorPagina;
	int primeiroRegistro;
	int ultimoRegistro;
	int total;

	public Paginacao(Integer qtdePorPagina, Integer total, String pagina){

    	/*Verificando a quantidade de páginas*/
    	float totalF = (Float.parseFloat(total+"") /qtdePorPagina);
    	int totalInt = totalF > (int)totalF ? (int)totalF+1 : (int)totalF;

    	links = new Integer[ totalInt ];

    	/*Se a página não foi informada assume que está na página 1*/
    	pagina = (pagina==null || pagina.equals("")) ? "1" : pagina;
    	paginaAtual = Integer.valueOf(pagina);

    	/*Setando as variáveis usadas para controle da paginação*/
    	this.qtdePorPagina = qtdePorPagina;
    	this.total = total;

    	firstResult = (paginaAtual-1) * qtdePorPagina;
    	primeiroRegistro = total > 0 ? firstResult + 1 : 0;
    	ultimoRegistro = (firstResult + qtdePorPagina) > total ? total : (firstResult + qtdePorPagina);

	}

	public Integer[] getLinks() {
		return links;
	}

	public void setLinks(Integer[] links) {
		this.links = links;
	}

	public int getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(int paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public int getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getQtdePorPagina() {
		return qtdePorPagina;
	}

	public void setQtdePorPagina(int qtdePorPagina) {
		this.qtdePorPagina = qtdePorPagina;
	}

	public int getPrimeiroRegistro() {
		return primeiroRegistro;
	}

	public void setPrimeiroRegistro(int primeiroRegistro) {
		this.primeiroRegistro = primeiroRegistro;
	}

	public int getUltimoRegistro() {
		return ultimoRegistro;
	}

	public void setUltimoRegistro(int ultimoRegistro) {
		this.ultimoRegistro = ultimoRegistro;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public String getProximaPagina(){
		if( ultimoRegistro < total )
			return (paginaAtual+1)+"";

		return "";
	}
	
	public String getAnteriorPagina(){
		if( primeiroRegistro > 1 )
			return "" + (paginaAtual+1);

		return "";
	}
	
}
