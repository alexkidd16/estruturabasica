package br.com.jdt.interceptors;

import br.com.jdt.factory.UsuarioDAO;
import br.com.jdt.modelagem.Usuario;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class VerificaLogin implements Interceptor{  
	public String intercept(ActionInvocation next) throws Exception {  
		Usuario usuario = (Usuario)next.getInvocationContext().getSession().get("usuario");
		
		String s="";
		if(usuario!=null){
			Usuario usuarioBD = UsuarioDAO.get( usuario.getId() );	
			s = next.invoke();
		}else
			s = "login";

		return s;
	}

	public void init() {
	}

	public void destroy() {
	}
}
