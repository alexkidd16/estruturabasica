package br.com.jdt.actions;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;

import br.com.jdt.factory.UsuarioDAO;
import br.com.jdt.modelagem.Usuario;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class Login extends ActionSupport{
	private String username;
	private String password;
	public String msg;
	
	public String execute() throws Exception {
//    	HttpServletResponse response = (HttpServletResponse) ActionContext.getContext().get(StrutsStatics.HTTP_RESPONSE);
    	ServletActionContext.getContext().getSession().clear();
    	Usuario usuario = UsuarioDAO.getUsuario( username , password );

    	if( usuario!=null ){
        	ServletActionContext.getContext().getSession().put( "usuario" , usuario );
    		return SUCCESS;
    	}else{
    		msg="Usuario não encontrado";
    		return ERROR;
    	}
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
