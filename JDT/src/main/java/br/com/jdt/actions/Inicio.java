package br.com.jdt.actions;

import org.apache.struts2.ServletActionContext;
import br.com.jdt.modelagem.Usuario;
import com.opensymphony.xwork2.ActionSupport;

public class Inicio extends ActionSupport{
	Usuario usuario;
	
	public String execute() throws Exception {
		usuario = (Usuario)ServletActionContext.getContext().getSession().get("usuario");
		
		return SUCCESS;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
