<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JDT | Bem Vindo!</title>

<link  href="<s:url value="/css/geral.css" />" rel="stylesheet" type="text/css"  />
<link  href="../css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<script src="//code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<s:url value="/js/topoAux.js"/>" ></script>
<script type="text/javascript" src="<s:url value="/js/admin.js"/>" ></script>

</head>

<body>
	<div id="topo">
    	<div id="meio">
        	<div class="logo">
                <ul id="jsddm">
					<li><a href="#">Sistema</a>
						<ul>
							<li><span class="a" onClick="carrega('list-usuario')">Usu&aacute;rios</span></li>
						</ul>
					</li>
				</ul>
				<div class="clear"></div>
            </div>
            <div class="user">
            	<table border="0" cellpadding="0" cellspacing="0">
                	<tr>
                    	<td>
            				<img alt="login" src="<s:url value="/images/logo_graf.jpg" />" width="24" height="24"  border="0"/>
                		</td>
                        <td>
                			&nbsp; Ol&aacute; <s:property value="usuario.nome" />  &nbsp; | &nbsp; <a href="<s:url value="/login/" />">Logout</a>
                        </td>
                	</tr>
                </table>
            </div>
        </div>
    </div>

    <div id="miolo">     	
    </div>
</body>
</html>
