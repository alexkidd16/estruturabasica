<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript" src="<s:url value="/js/usuarios.js" />" ></script>
<div id="titulo">
  	<div class="img-titulo"><img src="<s:url value="/images/icons/flag_24x24.png" />" /></div>
      <div class="titulo-combo"><i>Usuário</i></div>
      <div class="buscar" style="width: 342px ; border:0px solid #000;" > 
      	<table border="0" cellpadding="2" cellspacing="0">
          	<tr>
				  <td>
                  	<input type="text" name="busca" id="txtBusca" class="input-style" style="width:240px" />
                  </td>
                  <td align="right">
                  	<input type="button" id="btnBuscarUsuario" value="Buscar" class="btn-buscar" />
                  </td>
          	</tr>
          </table>          
      </div>
  </div>
  <span onClick="carrega('novo-usuario');">Novo</span>

<div class="lista-pedido">
<br/>
      <table border="0" width="920" cellpadding="3" cellspacing="4" style="border:0px solid #CCC;">
         <tr>
             <td class="dados" width="40" align="left">
             	<b>Codigo</b>
             </td>
             <td class="dados" width="380">
             	<b>Nome</b>
             </td>
             <td class="dados" width="90">
             	<b>Data Entrada</b>
             </td>
              <td class="dados" width="40">
             	<b>A&ccedil;&otilde;es</b>
             </td>
         </tr>
         
		<s:iterator value="usuarios" status="stat">
     	<tr id="usuario-<s:property value="id"/>" class="rover">
             <td class="dados" width="45" align="left">
            		<s:property value="id"/>
             </td>
             <td class="dados" width="90" align="left">
            	<s:property value="nome"/>
             </td>
             <td class="dados" width="110" align="left">
            	<s:date name="dataEntrada" format="dd/MM/yyyy hh:mm a" />
             </td>
              <td class="dados" width="50" align="left"  id="edit">
             	<img style="cursor:pointer" onClick="carrega('edit-usuario?id=<s:property value="id"/>');" width="15px" height="15px" src="<s:url value="/images/icons/edit_24x24.png" />" title="Editar Usu&aacute;rio"/> 
             	<img style="cursor:pointer" onClick="excluirUsuario('<s:property value="id"/>');" width="15px" height="15px" src="<s:url value="/images/icons/delete_24x24.png" />" title="Excluir Usu&aacute;rio"/>
             </td>
        </tr>
		</s:iterator>
     </table>
</div>

<div id="titulo" style="margin-top: 15px;">
<div class="titulo-combo"><i>Resultados <s:property value="paginacao.primeiroRegistro"/> - <s:property value="paginacao.ultimoRegistro"/> de <s:property value="paginacao.total"/></i></div>
	<div class="buscar" style="width: 400px;font-size: 12px;padding: 10px 10px 0 0;text-align: right;"> 
	Paginas :: 	<s:iterator value="paginacao.links" status="stat">
					<a href="javascript:carrega('list-usuario', {pagina : '<s:property value="#stat.count"/>', 'busca':'<s:property value="busca"/>'});"> <s:property value="#stat.count"/> </a>
				</s:iterator>
	</div>
</div>