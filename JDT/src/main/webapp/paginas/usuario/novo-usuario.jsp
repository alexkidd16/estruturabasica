<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript" src="<s:url value="/js/usuarios.js" />"></script>
<div id="titulo">
	<div class="img-titulo">
		<img src="<s:url value="/images/icons/document_24x24.png" />" />
	</div>
	<div class="titulo-combo">
		<i>Usuários</i>
	</div>
	<div class="buscar" style="border: 0px solid #ddd; width: 340px;">
	</div>
</div>

<div style="width: 920px; margin-top: 10px; margin-bottom: 15px;">
	<table width="920" border="0px">
		<s:if test="editar==true">
			<tr>
				<td class="dados">ID:</td>
				<td>
					<input type="text" id="txtId" class="input-style" readonly style="width: 360px;" disabled value="<s:property value="usuario.id"/>" />
				</td>
			</tr>
			<tr>
				<td class="dados">Data de Entrada:</td>
				<td>
					<input type="text" class="input-style" readonly	style="width: 360px;" disabled value="<s:date name="usuario.dataEntrada" format="dd/MM/yyyy hh:mm a" />" />
				</td>
			</tr>
		</s:if>
		<tr>
			<td class="dados">Login:</td>
			<td>
				<input type="text" name="nome" id="txtLogin" class="input-style" value="<s:property value="usuario.login"/>" style="width: 360px;" />
			</td>
		</tr>
		<tr>
			<td class="dados">Senha:</td>
			<td>
				<input type="password" name="nome" id="txtSenha" class="input-style" style="width: 360px;" />
			</td>
		</tr>

		<tr>
			<td class="dados">Nome:</td>
			<td>
				<input type="text" name="nome" id="txtNome"	class="input-style" value="<s:property value="usuario.nome"/>" style="width: 360px;" />
			</td>
		</tr>

		<tr>
			<td class="dados">Email:</td>
			<td>
				<input type="text" name="nome" id="txtEmail" class="input-style" value="<s:property value="usuario.email"/>" style="width: 360px;" />
			</td>
		</tr>

		<tr>
			<td colspan=2 align="right">
				<span id="msg" class="msgAviso"></span>&nbsp;&nbsp;&nbsp;
				<input type="button" id="btnAdicionarUsuario" value="<s:if test="editar==true">Editar</s:if><s:else>Adicionar</s:else>"	class="btn-buscar" />
			</td>
		</tr>
	</table>
</div>