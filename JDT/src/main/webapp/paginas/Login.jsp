<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>JDT | Login</title>
<link  href="../css/login.css" rel="stylesheet" type="text/css"  />
</head>
<body>

<div id="area-login" align="center">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<br />

<!-- FORMULARIO DE LOGIN -->

<s:form action="Login">
<table border="0">
  <tr>
    <td colspan="2" align="center">LOGIN</td>
    </tr>
  
  <tr>
    <td align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
	<s:textfield label="USUARIO" name="username" class="inp150"></s:textfield> 
  </tr>
  <tr>
    <td align="right"></td>
    <td align="left"></td>
  </tr>
  <tr>
    <s:password name="password" label="SENHA" class="inp150"></s:password>
  </tr>
  
  <tr>
    <td colspan="2" align="right"><span class="errorMessage"><s:property value="msg" /></span></td>
  </tr>
  
    <tr>
    <td align="right">&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <s:submit value=" " class="btnSubmit" />
</table>
</s:form>

<!-- FIM DO FORMULARIO DE LOGIN -->

<p>&nbsp;</p>
<p>&nbsp;</p>
</div>

<!--FIM DA AREA DE LOGIN-->
</body>
</html>
