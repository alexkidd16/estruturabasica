$(document).ready(function(){
	$("#btnBuscarUsuario").click(function(){
		$.post( "list-usuario", { 'busca' : $("#txtBusca").attr("value") } , function(data){
			$("#miolo").html( data );
		});
	});
	
	$("#txtBusca").keypress(function( e ){
		if ( e.which == 13 ) {
			$.post( "list-usuario", { 'busca' : $("#txtBusca").attr("value") } , function(data){
				$("#miolo").html( data );
			});
		}
	});
	
	$("#btnAdicionarUsuario").click(function(){
		$.post( "add-usuario", { 'login' : $("#txtLogin").attr("value"), 'senha' : $("#txtSenha").attr("value"), 'nome':$("#txtNome").attr("value"),  'email':$("#txtEmail").attr("value"), 'id': $("#txtId").attr("value") } , function(data){
			msgErro = eval($("erro", data).text());
			msgResposta = $("mensagem", data).html();
			msgInformacao = $("informacao", data).text();

			if(msgErro == '0' ){
				mostraMensagem("Usuário editado com sucesso");
			}else if(msgErro=="1"){
				mostraMensagem( msgInformacao );
			}else{
				mostraMensagem("Ocorreu um erro ao fazer a edição");
			}
		});
	});
});

function excluirUsuario(id){
	if(!confirm("Confirma exclusão?"))
		return ;
	
	$.post( "delete-usuario", { 'id': id } , function(data){
		msgErro = eval($("erro", data).text());
		msgResposta = $("mensagem", data).html();
		msgInformacao = $("informacao", data).text();

		if(msgErro == '0' ){
			$("#usuario-" + id).fadeOut();
		}else if(msgErro=="1"){
			mostraMensagem( msgInformacao );
		}else{
			mostraMensagem("Ocorreu um erro ao fazer a exclusão");
		}
	});
}

