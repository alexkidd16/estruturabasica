function carregaSemParametros(pagina){
	$('#miolo').html("<div class='carregando-load' align='center'> <img src='../images/load.gif'/> <br/> <b>CARREGANDO</b></div>");
	$("#miolo").load( pagina);
}

function mostraMensagem(msg){
	$("#msg").fadeOut('normal', function(){
		$("#msg").html(msg);
		$("#msg").fadeIn();
	})
}

function carrega(pagina, parametros){
	if(parametros==undefined)
		return carregaSemParametros(pagina);
	
	if(!parametros.edicao)
		$('#miolo').html("<div class='carregando-load' align='center'> <img src='../images/load.gif'/> <br/> <b>CARREGANDO</b></div>");
	else
		$("#" + parametros.dvAlternativa).html("<div class='carregando-load' align='center'> <img src='../images/load.gif'/> <br/> <b>CARREGANDO</b></div>");
	
	
	$.post(pagina , parametros, function(data){
		if(!parametros.edicao)
			$('#miolo').html(data);
		else
			$("#" + parametros.dvAlternativa).html(data);
		
	});
}